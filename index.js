// CRUD Operations (Create, Read, Update, Delete)


/*

	- CRUD operations are the heart of any backend application
	- Mastering the CRUD operations is essential for any developer
	- This helps in building character and increasing exposure to logical statements that will help us manipulate our date.
	- Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information

*/

// [SECTION] Inserting Documents (CREATE)
/*
	Syntax:
	- db.collectionName.insertOne({object});
*/

// Insert One (Inserts one document)

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87612345",
		email: "janedoe@mail.com",
	},
	courses: ["CSS","JavaScript", "Phyton"],
	department: "none",
})

// Insert Many (Inserts one or more document)
/*
	- Syntax
	- db.collectionName.insertMany([ {object A}, {objectB} ]);
*/

db.users.insertMany(
	[
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87000",
				email: "stephenhawking@mail.com",
			},
			courses: ["Phython", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "4770220",
				email: "neilarmstrong@mail.com",
			},
			courses: ["React", "Laravel", "MongoDB"],
			department: "none"
		}
	]
)

// [SECTION] Finding Documents (READ)

/*
	-db.collectionName.find();
	-db.collectionName.find({field: value});
*/

// retrieves all documents
db.users.find();

// retries specific document/s
db.users.find({firstName: "Stephen"});
// multiple criteria
db.users.find({lastName: "Armstrong", age:82});


// [SECTION] Update and Replace (UPDATE)

// Inserting something we want to update

db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});
/*
	Syntax:
	- db.collectionName.update({criteria}. {$set: {fields: value/s}})

*/
db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "imrich@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)

db.users.updateOne(
	{firstName: "Jane"},
	{
		$set:
		{
			firstName: "Jenny"
		}
	}

)

// updating multiple documents that matches field and value / criteria
db.users.updateMany(
	{department: "none"},
	{
		$set:
		{
			department: "HR"
		}
	}

)


// replaces all the fields in a document/object
db.users.replaceOne(
	{firstName: "Bill"}, // criteria
	{
		firstName: "Elon",
		lastName: "Musk",
		age: 30,
		courses: ["PHP", "Laravel", "HTML"],
		status: "trippings"
	}
)




// [SECTION] Delete(Delete)

/*
	db.collectionName.deleteOne({field: value});
	db.collectionName.deleteMany({field: value});
*/

db.users.deleteOne({ // deletes one document that matches the field and value
	firstName: "Jane"
})

db.users.deleteMany({ // deletes all docmuent that matches the field and value
	firstName: "Niel"
})

db.users.deleteMany({
	department: "HR"
})

db.hotel.find();